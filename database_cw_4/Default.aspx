﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="database_cw_4._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <style>
    .card {
      margin: 20px;
      height: 350px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    .card-header {
      text-align: center;
      font-size: 40px;
    }
    .card-footer {
      background-color: #333;
      color: #fff;
      text-align: center;
    }
    .view-details {
      color: #fff;
      text-decoration: none;
    }
    .view-details:hover {
      color: #ccc;
    }
  </style>

<div class="container">
  <div class="row">

    <div class="col-md-4">
      <div class="card" style="background-color: #FF4136">
        <div class="card-header">Employee Details</div>
        <div class="card-footer">
          <a href="/EmployeeDetails" class="view-details">
            View details <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="card" style="background-color: #0074D9">
        <div class="card-header">Department Details</div>
        <div class="card-footer">
          <a href="/DepartmentDetails" class="view-details">
            View details <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="card" style="background-color: #2ECC40">
        <div class="card-header">Job Details</div>
        <div class="card-footer">
          <a href="/JobDetails" class="view-details">
            View details <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="card" style="background-color: #FF851B">
        <div class="card-header">Role Details</div>
        <div class="card-footer">
          <a href="/RoleDetails" class="view-details">
            View details <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>

      <div class="col-md-4">
      <div class="card" style="background-color: #B10DC9">
        <div class="card-header">Address Details</div>
        <div class="card-footer">
          <a href="/AddressDetails" class="view-details">
            View details <i class="fa fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>


      </div>
    </div>


</asp:Content>
